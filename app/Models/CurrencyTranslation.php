<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CurrencyTranslation
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $language_id
 * @property int $currency_id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|CurrencyTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CurrencyTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CurrencyTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|CurrencyTranslation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CurrencyTranslation whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CurrencyTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CurrencyTranslation whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CurrencyTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CurrencyTranslation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CurrencyTranslation extends Model
{
    use HasFactory;
}
