<?php

namespace App\Http\Livewire\Admin;

use App\Http\Livewire\Events;
use Livewire\Component;

class Login extends Component
{
    public string $email = '';
    public string $password = '';
    public bool $remember = false;
    public bool $showFailedLogin = false;

    protected array $rules = [
        'email' => 'required|email',
        'password' => 'required',
    ];

    public function render()
    {
        return view('livewire.admin.login');
    }

    function updated(string $propertyName): void
    {
        $this->validateOnly($propertyName);
        $this->showFailedLogin = false;
    }

    function submit(): void
    {
        $isSuccessful = auth()->guard('admin')->attempt(
            ['email' => $this->email, 'password' => $this->password],
            $this->remember,
        );
        $this->showFailedLogin = !$isSuccessful;
        if ($isSuccessful) {
            $this->emit(Events::ADMIN_LOGGED_IN);
        }
    }
}
