<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\LocationProduct
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $product_id
 * @property int $location_id
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProduct newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProduct newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProduct query()
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProduct whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProduct whereLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProduct whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LocationProduct whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class LocationProduct extends Model
{
    use HasFactory;
}
