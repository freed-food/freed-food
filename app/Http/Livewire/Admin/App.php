<?php

namespace App\Http\Livewire\Admin;

use App\Http\Livewire\Events;
use Livewire\Component;

class App extends Component
{
    protected $listeners = [
        Events::ADMIN_LOGGED_IN => 'noop',
    ];

    public function render()
    {
        return view(
            'livewire.admin.app',
            [
                'isLoggedIn' => auth()->guard('admin')->check()
            ]
        )->layout('layouts.admin.app');
    }

    function noop(): void
    {
    }
}
