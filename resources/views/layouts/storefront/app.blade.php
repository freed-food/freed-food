<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Storefront</title>

    @livewireStyles
    @vite('resources/css/storefront/storefront.css')
</head>
<body>
    {{$slot}}

    @livewireScripts
    @vite('resources/js/storefront/storefront.js')
</body>
</html>
