<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\LanguageTranslation
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $language_id
 * @property int $translation_language_id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|LanguageTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LanguageTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LanguageTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|LanguageTranslation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LanguageTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LanguageTranslation whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LanguageTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LanguageTranslation whereTranslationLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LanguageTranslation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class LanguageTranslation extends Model
{
    use HasFactory;
}
