<?php

namespace App\Http\Livewire\Storefront;

use Livewire\Component;

class App extends Component
{
    public function render()
    {
        return view('livewire.storefront.app')->layout('layouts.storefront.app');
    }
}
