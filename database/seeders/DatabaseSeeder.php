<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         \App\Models\User::factory()->create([
             'name' => 'Martin Schophaus',
             'email' => 'mschopdev@gmail.com',
             'password' => Hash::make('test'),
             'email_verified_at' => date('Y-m-d'),
         ]);
    }
}
