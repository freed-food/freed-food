<div class="container-fluid">
    @if ($isLoggedIn)
        <livewire:admin.area />
    @else
        <livewire:admin.login />
    @endif
</div>
