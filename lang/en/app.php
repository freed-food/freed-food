<?php return [
    'login-header' => 'Admin Login',
    'label' => [
        'email' => 'Email',
        'password' => 'Password',
        'login-remember' => 'Remember me'
    ],
    'button' => [
        'login' => 'Login Now',
    ]
];
