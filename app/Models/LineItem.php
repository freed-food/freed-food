<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\LineItem
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $order_id
 * @property int $product_id
 * @property string $price
 * @method static \Illuminate\Database\Eloquent\Builder|LineItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LineItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LineItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|LineItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LineItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LineItem whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LineItem wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LineItem whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LineItem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class LineItem extends Model
{
    use HasFactory;
}
