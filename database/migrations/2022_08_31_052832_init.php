<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages', function(Blueprint $table) {
            $table->id();
            $table->timestamps();
        });

        Schema::create('language_translations', function(Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('language_id')->references('id')->on('languages')->cascadeOnDelete();
            $table->foreignId('translation_language_id')->references('id')->on('languages')->cascadeOnDelete();
            $table->unique(['language_id', 'translation_language_id']);
            $table->string('name');
        });

        Schema::create('locales', function(Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('language_id')->references('id')->on('languages')->cascadeOnDelete();
            $table->string('code')->unique();
        });

        Schema::create('currencies', function(Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('code')->unique();
            $table->string('sign');
        });

        Schema::create('currency_translations', function(Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('language_id')->references('id')->on('languages')->cascadeOnDelete();
            $table->foreignId('currency_id')->references('id')->on('currencies')->cascadeOnDelete();

            $table->string('name');
        });

        Schema::create('countries', function(Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('code');
        });

        Schema::create('country_translations', function(Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('country_id')->references('id')->on('countries')->cascadeOnDelete();
            $table->foreignId('language_id')->references('id')->on('languages')->cascadeOnDelete();
            $table->unique(['country_id', 'language_id']);
            $table->string('name');
        });

        Schema::create('companies', function(Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name');
            $table->string('street');
            $table->string('house_no');
            $table->string('city');
            $table->string('zip_code');
            $table->string('vat_id');
        });

        Schema::create('locations', function(Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('company_id')->references('id')->on('companies');
            $table->string('name');
            $table->string('street');
            $table->string('house_no');
            $table->string('city');
            $table->string('zip_code');
        });

        Schema::create('products', function(Blueprint $table) {
            $table->id();
            $table->timestamps();
        });

        Schema::create('product_translations', function(Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('language_id')->references('id')->on('languages')->cascadeOnDelete();
            $table->foreignId('product_id')->references('id')->on('products')->cascadeOnDelete();
            $table->unique(['language_id', 'product_id']);
            $table->string('name');
        });

        Schema::create('location_products', function(Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('product_id')->references('id')->on('products')->cascadeOnDelete();
            $table->foreignId('location_id')->references('id')->on('locations')->cascadeOnDelete();
            $table->unique(['product_id', 'location_id']);
        });

        Schema::create('customers', function(Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at');
            $table->string('password');
            $table->rememberToken();
        });

        Schema::create('orders', function(Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('customer_id')->references('id')->on('customers');
            $table->foreignId('location_id')->references('id')->on('locations');
            $table->foreignId('currency_id')->references('id')->on('currencies');
        });

        Schema::create('line_items', function(Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('order_id')->references('id')->on('orders');
            $table->foreignId('product_id')->references('id')->on('products');
            $table->decimal('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('line_items');
        Schema::drop('orders');
        Schema::drop('customers');
        Schema::drop('location_products');
        Schema::drop('product_translations');
        Schema::drop('products');
        Schema::drop('locations');
        Schema::drop('companies');
        Schema::drop('country_translations');
        Schema::drop('countries');
        Schema::drop('currency_translations');
        Schema::drop('currencies');
        Schema::drop('locales');
        Schema::drop('language_translations');
        Schema::drop('languages');
    }
};
