<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Admin</title>

    @livewireStyles
    <link rel="stylesheet" href="https://cdn.metroui.org.ua/v4/css/metro-all.min.css">
    <script src="https://cdn.metroui.org.ua/v4/js/metro.min.js"></script>
    @vite('resources/css/admin/admin.css')
</head>
<body>
    {{$slot}}

    @livewireScripts
    @vite('resources/js/admin/admin.js')
</body>
</html>
