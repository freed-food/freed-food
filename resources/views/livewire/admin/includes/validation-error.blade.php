@error($field)
    <div class="validation-error">
        <span>🛈</span>
        {{$message}}
    </div>
@enderror
