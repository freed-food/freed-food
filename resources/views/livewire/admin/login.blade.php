<div class="m-login">
    <aside class="offcanvas active p-5">
        <form wire:submit.prevent="submit">
            <h2>{{__('app.login-header')}}</h2>
            <p>{{__('app.label.email')}}</p>
            <input type="email" wire:model.lazy="email" data-role="input" placeholder="max.mustermann@provider.de"/>
            @include('livewire/admin/includes/validation-error', ['field' => 'email'])
            <p><label for="login-password">{{__('app.label.password')}}</label></p>
            <input id="login-password" type="password" wire:model.lazy="password" data-role="input"/>
            <div>
                <input id="login-remember" type="checkbox" wire:model="remember" />
                <label for="login-remember">{{__('app.label.login-remember')}}</label>
            </div>
            @if($showFailedLogin)
                @include('livewire/admin/includes/message-bar', ['message' => __('app.login-failed')])
            @endif
            <button class="image-button primary mt-8" type="submit">
                <span class="mif-arrow-right icon"></span>
                <span class="caption">{{__('app.button.login')}}</span>
            </button>
        </form>
    </aside>
</div>
